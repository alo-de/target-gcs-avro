# target-gcs-avro

This is a [Singer](https://singer.io) target that reads JSON-formatted data
following the [Singer spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).

The way it works is we write data into a temporary file as Avro format and then upload
to Google Cloud Storage.

How to run

`target-gcs-avro --config path/to/your/config.json --date yyyy-mm-dd`

If `date` flag is not provided then we set the value to current date.


---

Copyright &copy; 2020 Alodokter
