#!/usr/bin/env python
from setuptools import setup

setup(
    name="target-gcs-avro",
    version="0.1.0",
    description="Singer.io target for GCS in Avro format",
    author="Alodokter Data Engineering",
    url="http://singer.io",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["target_gcs_avro"],
    install_requires=[
        "singer-python==5.9.0",
        "avro-python3==1.10.0"
    ],
    entry_points="""
    [console_scripts]
    target-gcs-avro=target_gcs_avro:main
    """,
    packages=["target_gcs_avro"],
    package_data = {},
    include_package_data=True,
)
