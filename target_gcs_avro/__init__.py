#!/usr/bin/env python3

import argparse
import io
import os
import sys
import json
import tempfile
from datetime import datetime

import singer

import avro.schema
from avro.datafile import DataFileWriter
from avro.io import DatumWriter

from google.cloud import storage

logger = singer.get_logger()


def emit_state(state):
    if state is not None:
        line = json.dumps(state)
        logger.debug('Emitting state {}'.format(line))
        sys.stdout.write("{}\n".format(line))
        sys.stdout.flush()


def get_gcs_bucket(target_bucket):
    gcs_client = storage.Client()

    try:
        gcs_client.get_bucket(target_bucket)
    except Exception as e:
        logger.exception(e)
        raise Exception("Please provide valid GCS bucket")

    bucket = gcs_client.get_bucket(target_bucket)
    return bucket


def persist_lines(config, lines, ingestion_date):
    bucket_name = config["bucket"]
    bucket = get_gcs_bucket(bucket_name)
    blob_name = config["blob"].format(ingestion_date=ingestion_date)

    tdir = config.get("tmp_dir", os.getcwd())
    if tdir:
        if not os.path.isdir(tdir):
            raise Exception("Path '{0}' does not exist!".format(tdir))
    
    avro_schema = None
    if config.get("avro_schema_path"):
        schema_path = config["avro_schema_path"]
        avro_schema = avro.schema.parse(open(schema_path, "rb").read())

    # Tap stream data processing
    state = None

    with tempfile.TemporaryDirectory(dir=tdir) as temp_dir:
        temp_filepath = os.path.join(temp_dir,
                                     "{}.avro".format(ingestion_date))
        avro_file = DataFileWriter(open(temp_filepath, "wb"),
                                        DatumWriter(),
                                        avro_schema)
        for line in lines:
            try:
                o = json.loads(line)
            except json.decoder.JSONDecodeError:
                logger.error("Unable to parse:\n{}".format(line))
                raise

            if 'type' not in o:
                raise Exception("Line is missing required key 'type': {}".format(line))

            t = o['type']

            if t == 'RECORD':
                if 'stream' not in o:
                    raise Exception("Line is missing required key 'stream': {}".format(line))

                avro_file.append(o['record'])

                state = None
            elif t == 'STATE':
                logger.debug('Setting state to {}'.format(o['value']))
                state = o['value']

        logger.info("Write data into {}".format(temp_filepath))
        avro_file.close()
        try:
            blob = bucket.blob(blob_name)
            storage.blob._DEFAULT_CHUNKSIZE = 2097152 # 1024 * 1024 B * 2 = 2 MB
            storage.blob._MAX_MULTIPART_SIZE = 2097152 # 2 MB

            with open(temp_filepath, 'rb') as f:
                blob.upload_from_file(f)
            #blob.upload_from_filename(temp_filepath)
        except Exception as e:
            logger.error(e)
    
    return state


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help='Config file')
    parser.add_argument('-dt', '--date', help='Ingestion date')
    args = parser.parse_args()

    if args.config:
        with open(args.config) as input:
            config = json.load(input)
    else:
        config = {}

    if args.date:
        ingestion_date = args.date
    else:
        ingestion_date = datetime.now().strftime("%Y-%m-%d")

    input = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    state = persist_lines(config, input, ingestion_date)
        
    emit_state(state)
    logger.debug("Exiting normally")


if __name__ == '__main__':
    main()
